# scoresheet -- a nicely displayed score sheet

This is a tcl program which reads round scores from a file and displays a nice
ascii score sheet.

This program is meant to be run with something like [entr][], adding a new entry
when a round is finished and then showing the other pane to display the current
total.

[entr]: https://github.com/eradman/entr

The file format is simple tsv. The first row are the player / team names, there
can be many of them, and in the following rows are the scores they gained / lost
in the individual rounds.

This program can't do anything clever like under / over the line scoring of
bridge or refas in preferans, you have to take care of such things yourself or
play games where the challenge is in the play and not in keeping score. There's
still room for improvement though.
